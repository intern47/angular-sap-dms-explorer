import { TestBed } from '@angular/core/testing';

import { DocumentFetchService } from './document-fetch.service';

describe('DocumentFetchService', () => {
  let service: DocumentFetchService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DocumentFetchService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
