import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-passport-form',
  templateUrl: './passport-form.component.html',
  styleUrls: ['./passport-form.component.css']
})
export class PassportFormComponent implements OnInit {
  myForm: FormGroup;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.myForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      middleName: ['', Validators.required],
      lastName: ['', Validators.required],
      gender: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      age: ['', Validators.required],
      nationality: ['', Validators.required],
      dateOfBirth: ['', Validators.required],
      countryOfBirth: ['', Validators.required],
      cityOfBirth: ['', Validators.required],
      stateOfBirth: ['', Validators.required],
      placeOfIssue: ['', Validators.required],
      dateOfIssue: ['', Validators.required],
      dateOfExpiration: ['', Validators.required],
      countryOfIssue: ['', Validators.required],
    });
  }

  onSubmit() {
    if (this.myForm.valid) {
      console.log(this.myForm.value);
      // You can handle form submission here
    }
  }

}
