import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface RenameDialogData {
  crewId: string;
  documentType: string;
  currentName: string;
  newName: string;
  docObjId : string;
}

@Component({
  selector: 'app-rename-dialog',
  templateUrl: './rename-dialog.component.html',
  styleUrls: ['./rename-dialog.component.css']
})
export class RenameDialogComponent implements OnInit {

  crewId: string;
  documentType: string;
  currentName: string;
  newName: string;
  docObjId : string;

  constructor(public dialogRef: MatDialogRef<RenameDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: RenameDialogData) {
    this.crewId = data.crewId;
    this.documentType = data.documentType;
    this.currentName = data.currentName;
    this.newName = data.newName;
    this.docObjId = data.docObjId;
  }

  ngOnInit(): void {
  }

  onSave(): void {
    const result = {
      crewId: this.crewId,
      documentType: this.documentType,
      currentName: this.currentName,
      newName: this.newName,
      docObjId : this.docObjId
    };
    if(result.newName != ''){
      this.dialogRef.close(result);
    } else {
      this.dialogRef.close();
    }
  }

  onClose(): void {
    this.dialogRef.close();
  }


}
