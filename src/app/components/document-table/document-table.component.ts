import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-document-table',
  templateUrl: './document-table.component.html',
  styleUrls: ['./document-table.component.css']
})
export class DocumentTableComponent implements OnInit {
  @Input() docData;
  constructor() { }

  ngOnInit(): void {
  }

}
