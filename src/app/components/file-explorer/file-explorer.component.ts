import { DocumentFetchService } from './../../shared/services/document-fetch.service';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { NgxSpinnerService } from 'ngx-spinner';
import { Clipboard } from '@angular/cdk/clipboard';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import  { RenameDialogComponent } from '../rename-dialog/rename-dialog.component';

// export interface DialogData {
//   animal: string;
//   name: string;
// }

export interface RenameDialogData {
  crewId: string;
  documentType: string;
  currentName: string;
  newName: string;
  docObjId : string;
}

@Component({
  selector: 'app-file-explorer',
  templateUrl: './file-explorer.component.html',
  styleUrls: ['./file-explorer.component.css'],
})

export class FileExplorerComponent implements OnInit {
  // animal: string;
  // name: string;
  navBarTitle: String = 'Document Explorer';
  folders: Object[];
  payload;
  resultFolders;
  folderName: String = '';
  folderPathArray = [];
  documentInfo = '';
  searchPlaceHolderText = 'Search Crew Id...';
  downloadStatus = 'Click To Download';
  // crewFolderVisibility: boolean = true;
  // documentTypeFolderVisibility: boolean = false;
  // documentFileVisibility: boolean = false;
  leftArrowVisibility: boolean = false;
  infoIconVisibility: boolean = false;
  path = '../../../assets/images/user.png';
  @ViewChild('downloadLink', { static: false }) downloadLink: ElementRef;

  constructor(
    private service: DocumentFetchService,
    private spinner: NgxSpinnerService,
    private snackBar: MatSnackBar,
    private clipboard: Clipboard,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.showLoading();
    this.folderName = 'Crew Id :';
    this.payload = {
      folderPath: 'Root/',
    };
    this.folderPathArray = this.payload.folderPath.split('/').slice(0, -1);
    this.getFolderDetails(this.payload);
  }

  getFolderDetails(payload) {
    this.showLoading();
    this.infoIconVisibility = false;
    let folderPath: string[] = payload.folderPath.split('/');
    if (folderPath.length === 2) {
      this.folderName = 'Crew Id:';
      this.leftArrowVisibility = false;
      // this.folderImageAndNameSwitch(true,false,false);
      this.path = '../../../assets/images/user.png';
      this.searchPlaceHolderText = 'Type To Search Crew Id\'s...';
    } else if (folderPath.length === 3) {
      this.folderName = 'Doc Type:';
      // this.folderImageAndNameSwitch(false,true,false);
      this.path = '../../../assets/images/folder.png';
      this.leftArrowVisibility = true;
      this.searchPlaceHolderText = 'Type to Search Folder\'s...';
    } else {
      this.leftArrowVisibility = true;
      this.folderName = 'Doc No:';
      // this.folderImageAndNameSwitch(false,false,true);
      if (folderPath[2] == 'PASSPORT') {
        this.path = '../../../assets/images/passport.png';
        this.infoIconVisibility = true;
        this.searchPlaceHolderText = 'Type To Search Passport\'s...';
      } else if (folderPath[2] == 'VISA') {
        this.path = '../../../assets/images/visa.png';
        this.infoIconVisibility = true;
        this.searchPlaceHolderText = 'Type To Search Visa\'s...';
      } else {
        this.path = '../../../assets/images/resident.png';
        this.infoIconVisibility = true;
        this.searchPlaceHolderText = 'Type To Search Resident\'s...';
      }
    }
    this.service.getFolderDetails(payload).subscribe((response: any) => {
      const folders = response.map((obj: any) => {
        return {
          ...obj,
          name: obj.name,
        };
      });
      console.log(folders);
      this.folders = folders;
      this.resultFolders = [...this.folders];
      this.hideLoading();
    });
  }

  folderChange(folder) {
    console.log(folder);
    if(!(folder.name.startsWith("front") || folder.name.startsWith("back"))){
      this.showLoading();
      this.payload.folderPath += folder.name + '/';
      this.folderPathArray = this.payload.folderPath.split('/').slice(0, -1);
      this.getFolderDetails(this.payload);
    }
  }

  // folderImageAndNameSwitch(crewFolder,documentTypeFolder,documentNameFile){
  //   this.crewFolderVisibility = crewFolder;
  //   this.documentTypeFolderVisibility = documentTypeFolder;
  //   this.documentFileVisibility = documentNameFile;
  // }

  previousFolder() {
    this.showLoading();
    let folderPath = '';
    folderPath = this.payload.folderPath.split('/').slice(0, -2).join('/');
    this.payload.folderPath = folderPath + '/';
    this.folderPathArray = this.payload.folderPath.split('/').slice(0, -1);
    this.getFolderDetails(this.payload);
  }

  onBreadcrumbItemClick(event: any): void {
    this.showLoading();
    let folderPath = '';
    const target = event.target || event.srcElement;
    const breadcrumbList = target.parentElement;
    const index = Array.prototype.indexOf.call(breadcrumbList.children, target);
    folderPath = this.payload.folderPath
      .split('/')
      .slice(0, index + 1)
      .join('/');
    this.payload.folderPath = folderPath + '/';
    this.folderPathArray = this.payload.folderPath.split('/').slice(0, -1);
    this.getFolderDetails(this.payload);
  }

  getDocumentInfo(documentObj) {
    this.documentInfo = 'Loading...';
    // console.log(documentObj);
    let payload = { objectId: documentObj.objectId };
    this.service.getDocumentInformation(payload).subscribe((response) => {
      // console.log(JSON.stringify(response, null, 2));
      let fileType = response.fileType;
      let size = +response.size / (1024 * 1024);
      let sizeInMB = size.toFixed(2);
      let createdOn = response.createdOn;
      this.documentInfo = `File Type: ${fileType}, File Size: ${sizeInMB} MB, Created On: ${createdOn}`;
    });
  }

  downloadDocument(documentObj) {
    this.downloadStatus = 'Downloading...';
    let payload = { objectId: documentObj.objectId };
    this.service.getDocumentBase64(payload).subscribe((response: any) => {
      this.downloadImage(response.base64, documentObj.name);
      this.openSnackBar('Download Completed','Ok');
      this.downloadStatus = 'Click To Download';
    });
  }

  downloadImage(base64, docName) {
    const blob = this.base64ToBlob(base64);
    const url = URL.createObjectURL(blob);

    const anchor = this.downloadLink.nativeElement;
    anchor.href = url;
    anchor.download = docName;
    anchor.click();

    URL.revokeObjectURL(url);
  }

  private base64ToBlob(base64Data: string): Blob {
    const byteString = atob(base64Data);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const uint8Array = new Uint8Array(arrayBuffer);

    for (let i = 0; i < byteString.length; i++) {
      uint8Array[i] = byteString.charCodeAt(i);
    }

    return new Blob([uint8Array], { type: 'image/png' });
  }

  onSearchValueReceived(searchText) {
    this.resultFolders = this.folders.filter(
      (obj: any) =>
        obj.hasOwnProperty('name') &&
        obj.name.includes(searchText.toUpperCase())
    );
  }

  copyToClipboard(objectId: string) {
    this.clipboard.copy(objectId);
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000, panelClass: 'custom-snackbar', verticalPosition: 'top'
    });
  }

  openRenameDialog(folder): void {
    const renameDialogData: RenameDialogData = {
      crewId: this.folderPathArray[1],
      documentType: this.folderPathArray[2],
      currentName: folder.name,
      newName: '',
      docObjId : folder.objectId
    };

    const dialogRef = this.dialog.open(RenameDialogComponent, {
      width: '300px',
      data: renameDialogData
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
      if(result != undefined){
        this.showLoading();
        this.renameDocument(result.docObjId, result.newName);
      }
    });

  }

  renameDocument(objectId,name){
    let payload = {objectId : objectId, name: name}
    this.service.renameDocument(payload).subscribe((response)=>{
      this.openSnackBar('Document name updated successfully','Ok');
      console.log(response);
      this.getFolderDetails(this.payload);
    })
  }

  deleteDocument(objectId){
    this.showLoading();
    let payload = {objectId : objectId}
    this.service.deleteDocument(payload).subscribe((response)=>{
      console.log(response);
      this.openSnackBar('Document deleted successfully','Ok');
      this.getFolderDetails(this.payload);
    })
  }

  showLoading() {
    this.spinner.show();
  }

  hideLoading() {
    this.spinner.hide();
  }
}
